$(document).ready(function() {
	$('#fullcalendar').fullCalendar({
		header: {
			left: '',
			center: 'custom1, custom2, custom3, custom4, custom5, custom6, custom7',
			right: ''
		},
		customButtons: {
			custom1: {
				text: 'Май',
				click: function() {
				// 	var moment = $('#fullcalendar').fullCalendar('getDate');
				// 	var currentMonth =  moment.format('YYYY')+'-'+05+'-'+05;
					$('#fullcalendar').fullCalendar('gotoDate', '2018-05-01');
					$(this).addClass('active').siblings().removeClass('active');

				}
			},
			custom2: {
				text: 'Июнь',
				click: function() {
					$('#fullcalendar').fullCalendar('gotoDate', '2018-06-01');
					$(this).addClass('active').siblings().removeClass('active');

				}
			},
			custom4: {
				text: 'Июль',
				click: function() {
					$('#fullcalendar').fullCalendar('gotoDate', '2018-07-01');
					$(this).addClass('active').siblings().removeClass('active');

				}
			},
			custom5: {
				text: 'Август',
				click: function() {
					$('#fullcalendar').fullCalendar('gotoDate', '2018-08-01');
					$(this).addClass('active').siblings().removeClass('active');

				}
			},
			custom6: {
				text: 'Сентябрь',
				click: function() {
					$('#fullcalendar').fullCalendar('gotoDate', '2018-09-01');
					$(this).addClass('active').siblings().removeClass('active');

				}
			},
			custom7: {
				text: 'Октябрь',
				click: function() {
					$('#fullcalendar').fullCalendar('gotoDate', '2018-10-01');
					$(this).addClass('active').siblings().removeClass('active');

				}
			}
		},
		locale: 'ru',
		firstDay: 1,
		fixedWeekCount: false,
		// showNonCurrentDates: false,
		defaultDate: '2018-05-12',
		dayNamesShort: ['Воскресенье', 'Понедельник', 'Вторник', 'Среда', 'Четверг', 'Пятница', 'Суббота'],
		editable: false,
		selectable: false,
		selectHelper: false,
		eventClick: function (calEvent, jsEvent, view) {
			// closePopovers();
			popoverElement = $(jsEvent.currentTarget);

			var items = $('#popover-wrapper').find('.popover'),
					itemsNums = items.length;
			console.log(itemsNums);

			if (itemsNums > 0) {
				$('#popover-wrapper').addClass('active');
			} else{
				$('#popover-wrapper').removeClass('active');
			}

		},
		eventRender: function (event, element) {
			element.popover({
				content: '<div class="popover-image">' +
									'<img src="uploads/' + event.image + '" alt="' + event.name + '">' +
										'<div class="popover__title"> ' + event.name + ' </div>' +
										'<div class="popover__label">'+ event.days +'</div>' +
								'</div>' +
								'<div class="popover-body">' +
									'<div class="popover-body__group">'+ event.group +'</div>' +
									'<div class="popover-body__place">' +
										'Свободно:' +
										'<span>' + event.place + '</span>' +
									'</div>' +
									'<a class="popover-body__button" href="' + event.href + '">Я участвую!</a>' +
								'</div>',
				placement: 'top',
				html: 'true',
				trigger: 'click',
				animation: 'true',
				container: '#popover-wrapper'
			});
		},
		events: [
			{
				title: 'Чуя + Катунь 3 дня',
				start: '2018-05-01',
				end: '2018-05-05',
				color: 'rgba(198, 201, 38, 0.3)',
				name: 'Чуя Катунь',
				days: '3 дня',
				image: 'alloy-7.jpg',
				group: 'Группа из 3 человек',
				place: '2 места',
				href: '#link2'
			},
			{
				title: 'Нижняя Катунь 3 дня',
				start: '2018-05-01',
				end: '2018-05-02',
				color: 'rgba(208, 2, 27, 0.2)',
				name: 'Нижняя Катунь',
				days: '3 дня',
				image: 'alloy-1.jpg',
				group: 'Группа из 6 человек',
				place: '4 места',
				href: '#link1'
			},
			{
				title: 'Средняя Катунь 1 день',
				start: '2018-05-01',
				end: '2018-05-01',
				color: 'rgba(0, 0, 0, 0.1)',
				name: 'Средняя Катунь',
				days: '1 день',
				image: 'alloy-5.jpg',
				group: 'Группа из 10 человек',
				place: '3 места',
				href: '#link3'
			},
			{
				title: 'Чуя+Башкаус 7 дней',
				start: '2018-05-03',
				end: '2018-05-05',
				color: 'rgba(187, 107, 217, 0.2)',
				name: 'Чуя Башкаус',
				days: '7 дней',
				image: 'alloy-5.jpg',
				group: 'Группа из 10 человек',
				place: '3 места',
				href: '#link3'
			},
			{
				title: 'Средняя Катунь 1 день',
				start: '2018-05-06',
				end: '2018-05-07',
				color: 'rgba(0, 0, 0, 0.1)',
				name: 'Средняя Катунь',
				days: '1 день',
				image: 'alloy-5.jpg',
				group: 'Группа из 10 человек',
				place: '3 места',
				href: '#link3'
			},
			{
				title: 'Чуя + Катунь 3 дня',
				start: '2018-05-07',
				end: '2018-05-11',
				color: 'rgba(198, 201, 38, 0.3)',
				name: 'Чуя Катунь',
				days: '3 дня',
				image: 'alloy-7.jpg',
				group: 'Группа из 3 человек',
				place: '2 места',
				href: '#link2'
			},
			{
				title: 'Средняя Катунь 1 день',
				start: '2018-05-09',
				end: '2018-05-09',
				color: 'rgba(0, 0, 0, 0.1)',
				name: 'Средняя Катунь',
				days: '1 день',
				image: 'alloy-5.jpg',
				group: 'Группа из 10 человек',
				place: '3 места',
				href: '#link3'
			},
			{
				title: 'Чуя+Башкаус 7 дней',
				start: '2018-05-11',
				end: '2018-05-13',
				color: 'rgba(187, 107, 217, 0.2)',
				name: 'Чуя Башкаус',
				days: '7 дней',
				image: 'alloy-5.jpg',
				group: 'Группа из 10 человек',
				place: '3 места',
				href: '#link3'
			},
			{
				title: 'Средняя Катунь 1 день',
				start: '2018-05-11',
				end: '2018-05-13',
				color: 'rgba(0, 0, 0, 0.1)',
				name: 'Средняя Катунь',
				days: '1 день',
				image: 'alloy-5.jpg',
				group: 'Группа из 10 человек',
				place: '3 места',
				href: '#link3'
			},
			{
				title: 'Нижняя Катунь 3 дня',
				start: '2018-05-11',
				end: '2018-05-11',
				color: 'rgba(208, 2, 27, 0.2)',
				name: 'Нижняя Катунь',
				days: '3 дня',
				image: 'alloy-1.jpg',
				group: 'Группа из 6 человек',
				place: '4 места',
				href: '#link1'
			},
			{
				title: 'Средняя Катунь 1 день',
				start: '2018-05-14',
				end: '2018-05-14',
				color: 'rgba(0, 0, 0, 0.1)',
				name: 'Средняя Катунь',
				days: '1 день',
				image: 'alloy-5.jpg',
				group: 'Группа из 10 человек',
				place: '3 места',
				href: '#link3'
			},
			{
				title: 'Нижняя Катунь 3 дня',
				start: '2018-05-15',
				end: '2018-05-16',
				color: 'rgba(208, 2, 27, 0.2)',
				name: 'Нижняя Катунь',
				days: '3 дня',
				image: 'alloy-1.jpg',
				group: 'Группа из 6 человек',
				place: '4 места',
				href: '#link1'
			},
			{
				title: 'Средняя Катунь 1 день',
				start: '2018-05-16',
				end: '2018-05-16',
				color: 'rgba(0, 0, 0, 0.1)',
				name: 'Средняя Катунь',
				days: '1 день',
				image: 'alloy-5.jpg',
				group: 'Группа из 10 человек',
				place: '3 места',
				href: '#link3'
			},
			{
				title: 'Средняя Катунь 1 день',
				start: '2018-05-18',
				end: '2018-05-21',
				color: 'rgba(0, 0, 0, 0.1)',
				name: 'Средняя Катунь',
				days: '1 день',
				image: 'alloy-5.jpg',
				group: 'Группа из 10 человек',
				place: '3 места',
				href: '#link3'
			},
			{
				title: 'Чулышман 5 дней',
				start: '2018-05-18',
				end: '2018-05-23',
				color: 'rgba(74, 144, 226, 0.4)',
				name: 'Чулышман',
				days: '5 дней',
				image: 'alloy-3.jpg',
				group: 'Группа из 7 человек',
				place: '1 место',
				href: '#link7'
			},
			{
				title: 'Средняя Катунь 1 день',
				start: '2018-05-23',
				end: '2018-05-23',
				color: 'rgba(0, 0, 0, 0.1)',
				name: 'Средняя Катунь',
				days: '1 день',
				image: 'alloy-5.jpg',
				group: 'Группа из 10 человек',
				place: '3 места',
				href: '#link3'
			},
			{
				title: 'Средняя Катунь 1 день',
				start: '2018-05-25',
				end: '2018-05-25',
				color: 'rgba(0, 0, 0, 0.1)',
				name: 'Средняя Катунь',
				days: '1 день',
				image: 'alloy-5.jpg',
				group: 'Группа из 10 человек',
				place: '3 места',
				href: '#link3'
			},
			{
				title: 'Чуя+Башкаус 7 дней',
				start: '2018-05-23',
				end: '2018-05-25',
				color: 'rgba(187, 107, 217, 0.2)',
				name: 'Чуя Башкаус',
				days: '7 дней',
				image: 'alloy-5.jpg',
				group: 'Группа из 10 человек',
				place: '3 места',
				href: '#link3'
			},
			{
				title: 'Нижняя Катунь 3 дня',
				start: '2018-05-25',
				end: '2018-05-26',
				color: 'rgba(208, 2, 27, 0.2)',
				name: 'Нижняя Катунь',
				days: '3 дня',
				image: 'alloy-1.jpg',
				group: 'Группа из 6 человек',
				place: '4 места',
				href: '#link1'
			},
			{
				title: 'Средняя Катунь 1 день',
				start: '2018-05-27',
				end: '2018-05-28',
				color: 'rgba(0, 0, 0, 0.1)',
				name: 'Средняя Катунь',
				days: '1 день',
				image: 'alloy-5.jpg',
				group: 'Группа из 10 человек',
				place: '3 места',
				href: '#link3'
			},
			{
				title: 'Средняя Катунь 1 день',
				start: '2018-05-30',
				end: '2018-05-30',
				color: 'rgba(0, 0, 0, 0.1)',
				name: 'Средняя Катунь',
				days: '1 день',
				image: 'alloy-5.jpg',
				group: 'Группа из 10 человек',
				place: '3 места',
				href: '#link3'
			},
			{
				title: 'Средняя Катунь 1 день',
				start: '2018-06-01',
				end: '2018-06-01',
				color: 'rgba(0, 0, 0, 0.1)',
				name: 'Средняя Катунь',
				days: '1 день',
				image: 'alloy-5.jpg',
				group: 'Группа из 10 человек',
				place: '3 места',
				href: '#link3'
			},
			{
				title: 'Средняя Катунь 1 день',
				start: '2018-06-02',
				end: '2018-06-02',
				color: 'rgba(0, 0, 0, 0.1)',
				name: 'Средняя Катунь',
				days: '1 день',
				image: 'alloy-5.jpg',
				group: 'Группа из 10 человек',
				place: '3 места',
				href: '#link3'
			},
			{
				title: 'Чуя + Катунь 3 дня',
				start: '2018-05-30',
				end: '2018-06-04',
				color: 'rgba(198, 201, 38, 0.3)',
				name: 'Чуя Катунь',
				days: '3 дня',
				image: 'alloy-7.jpg',
				group: 'Группа из 3 человек',
				place: '2 места',
				href: '#link2'
			}
		]
	});

	function initDateButton(elem) {
		elem.addClass('active').siblings().removeClass('active');
	}

	initDateButton($('.fc-custom1-button'));

	var popoverElement;

	function closePopovers() {
		$('.popover').not(this).popover('hide');
		$('#popover-wrapper').removeClass('active');
	}
	$('body').on('click', function (e) {

		if (popoverElement && ((!popoverElement.is(e.target) && popoverElement.has(e.target).length === 0 && $('.popover').has(e.target).length === 0) || (popoverElement.has(e.target) && e.target.id === 'closepopover'))) {

				///$('.popover').popover('hide'); --> works
				closePopovers();
		}
	});
});
