/*----------------------------------------
	TABS
 ----------------------------------------*/

(function () {
	var clickEvent = ((document.ontouchstart!==null)?'click':'touchstart');

	var tabs = $('.js-tabs'),
			tabsPanel = $('.tabs-panel'),
			tab = $('.tabs-list-item a'),
			tabItem = $('.tabs-item '),
			tabButton = $('.tabs-panel__button');

	/* INIT */
	function initTab(elem) {
		$('#' + elem).addClass('active').siblings().removeClass('active');
		$("a[href='#" + elem + "']").addClass('active').parent().siblings().children('a').removeClass('active');
	};

	/* ACTIVE */
	function activeTab(elem) {
		var tabId = elem.attr('href');


		window.location.hash = tabId;
		elem.addClass('active').parent().siblings().children('a').removeClass('active');
		$(tabId).addClass('active').siblings().removeClass('active');
	};

	/* INIT TEXT BOTTOM */
	function initBotton() {
		var btn = $('.tabs-list-item a.active');
		btn.each(function() {
			var btnText = $(this).text();
			$(this).closest('.js-tabs').find('.tabs-panel__button').text(btnText);
		});
	}

	/* OPEN LIST */
	function openList(elem) {
		elem.parent().toggleClass('open');
		elem.toggleClass('clicked').siblings('.tabs-list').toggleClass('open');
	}

	/* CLOSE LIST */
	function closeList() {
		tabsPanel.removeClass('open');
		$('.tabs-list').removeClass('open');
		tabButton.removeClass('clicked');
	}

	/* FOR URLS */
	function initUrl() {
		var loc = window.location.hash.replace("#","");
		if (loc == "") {
			loc = "tab1"
		}
		initTab(loc);
	};

	/* CLOSE ON CLICK ON DOCUMENT */
	$(document).on(clickEvent, function(e) {
		if (!$(e.target).closest('.js-tabs').length) {
			closeList();
		}
		e.stopPropagation();
	});

	// initTab('tab3');
	initUrl();

	tabButton.on('click', function() {
		openList($(this));
	});

	tab.on('click', function(e) {
		e.preventDefault();
		activeTab($(this));
		initBotton();
		closeList();
	});

	initBotton();

})();
