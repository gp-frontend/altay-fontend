var collapseToggle = $('.collapse-header');

function openCollapse() {
	// debugger;
	$(this)
	.toggleClass('collapse-header_clicked')
	.siblings('.collapse-body')
	.toggleClass('collapse-body_active')
	.slideToggle('300');
}

collapseToggle.on('click', openCollapse) ;

/* TEXT */

var collapseToggleLink = $('.collapse-text__link');

function openCollapseText(e) {
	e.preventDefault();
	// debugger;
	$(this)
	.toggleClass('.collapse-text__link_clicked')
	.siblings('.collapse-text-body')
	.toggleClass('collapse-text-body_active')
	.slideToggle('300');

	if($(this).hasClass('.collapse-text__link_clicked')){
		$(this).text('Скрыть подробное описание сплава');
	}else{
		$(this).text('Показать подробное описание сплава');
	}
}

collapseToggleLink.on('click', openCollapseText) ;
